# A 2.4GHz MYSENSORS REMOTE COMMAND

The purpose of this page is to explain step by step the realization of a connected remote command based on ARDUINO PRO MINI, connected by radio to a DOMOTICZ server, using an NRF24L01 2.4GHZ module.

The board uses the following components :

 * an ARDUINO PRO MINI 3.3V 8MHz
 * a NRF24L01
 * an MCP23008 expander
 * an HT7533-1 3.3V regulator
 * some passive components

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.5.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2019/06/une-telecommande-domotique-24ghz.html
