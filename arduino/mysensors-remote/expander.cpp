
#include <Arduino.h>

#include "expander.h"

uint8_t MyMCP23008::read8(uint8_t addr) {
  Wire.beginTransmission(MCP23008_ADDRESS | i2caddr);
#if ARDUINO >= 100
  Wire.write((byte)addr);  
#else
  Wire.send(addr);  
#endif
  Wire.endTransmission();
  Wire.requestFrom(MCP23008_ADDRESS | i2caddr, 1);

#if ARDUINO >= 100
  return Wire.read();
#else
  return Wire.receive();
#endif
}

void MyMCP23008::write8(uint8_t addr, uint8_t data) {
  Wire.beginTransmission(MCP23008_ADDRESS | i2caddr);
#if ARDUINO >= 100
  Wire.write((byte)addr);
  Wire.write((byte)data);
#else
  Wire.send(addr);  
  Wire.send(data);
#endif
  Wire.endTransmission();
}

void MyMCP23008::enableInterrupts(uint8_t data)
{
  this->write8(MCP23008_INTCON, 0);
  this->write8(MCP23008_DEFVAL, 0);
  this->write8(MCP23008_GPINTEN, data);
}

void MyMCP23008::ackInterrupt(void)
{
  this->read8(MCP23008_INTCAP);
}

