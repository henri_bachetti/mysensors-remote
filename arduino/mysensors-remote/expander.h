
#ifndef _EXPANDER_H_
#define _EXPANDER_H_

#include <Wire.h>
#include "Adafruit_MCP23008.h"

class MyMCP23008 : public Adafruit_MCP23008
{
  protected:
    void write8(uint8_t addr, uint8_t data);
    uint8_t read8(uint8_t addr);
    uint8_t i2caddr;

  public:  
    void enableInterrupts(uint8_t data);
    void ackInterrupt(void);
};

#endif

