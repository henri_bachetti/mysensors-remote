
// Enable debug prints to serial monitor
#define MY_DEBUG

// Enable and select radio type attached
#define MY_RADIO_RF24
//#define MY_RADIO_RFM69
#define MY_RF24_CE_PIN        7
#define MY_RF24_CS_PIN        8

#include <MySensors.h>

#include <SPI.h>
#include "expander.h"

#define CHILD_ID              3
#define BUTTONS               8
#define MCP_INTERRUPT         3
#define SLEEP_TIME            900000L
#define bit_set(val, bit_no)  (((val) >> (bit_no)) & 1)
#define VREF                  1.15

MyMCP23008 mcp;
bool buttonState[BUTTONS];
MyMessage msg(CHILD_ID, V_STATUS);

struct batteryCapacity
{
  float voltage;
  int capacity;
};

const batteryCapacity remainingCapacity[] = {
  4.20,   100,
  4.10,   96,
  4.00,   92,
  3.96,   89,
  3.92,   85,
  3.89,   81,
  3.86,   77,
  3.83,   73,
  3.80,   69,
  3.77,   65,
  3.75,   62,
  3.72,   58,
  3.70,   55,
  3.66,   51,
  3.62,   47,
  3.58,   43,
  3.55,   40,
  3.51,   35,
  3.48,   32,
  3.44,   26,
  3.40,   24,
  3.37,   20,
  3.35,   17,
  3.27,   13,
  3.20,   9,
  3.1,    6,
  3.00,   3,
};

const int ncell = sizeof(remainingCapacity) / sizeof(struct batteryCapacity);

void before()
{
  Serial.begin(115200);
  Serial.println("before");
  mcp.begin();
  for (int button = 0 ; button < BUTTONS ; button++) {
    mcp.pinMode(button, INPUT);
    mcp.pullUp(button, HIGH);  // turn on a 100K pullup internally
  }
  pinMode(MCP_INTERRUPT, INPUT);
  mcp.enableInterrupts(0xff);
  mcp.ackInterrupt();
  analogReference(INTERNAL);
  Serial.println("OK");
}

void setup()
{
}

void presentation()
{
  Serial.println("presentation");
  for (int dev = 0 ; dev < BUTTONS ; dev++) {
    present(CHILD_ID + dev, S_BINARY);
  }
  Serial.println("OK");
}

unsigned int getBatteryCapacity(void)
{
  unsigned int adc = analogRead(0);
#ifdef MY_DEBUG
  Serial.print("ADC: ");
  Serial.println(adc);
#endif
  float voltage = adc * VREF / 1023 / 0.248;
#ifdef MY_DEBUG
  Serial.print("VCC: ");
  Serial.println(voltage, 3);
#endif
  for (int i = 0 ; i < ncell ; i++){
#ifdef MY_DEBUG
    Serial.print(i);
    Serial.print(" : ");
    Serial.print(remainingCapacity[i].voltage);
    Serial.print(" | ");
    Serial.println(remainingCapacity[i].capacity);
#endif
    if (voltage > remainingCapacity[i].voltage) {
      return remainingCapacity[i].capacity;
    }
  }
  return 0;
}

void loop()
{
  Serial.println("Sleep"); delay(10);
  sleep(digitalPinToInterrupt(MCP_INTERRUPT), FALLING, SLEEP_TIME);
  Serial.println("Interrupt");
  mcp.ackInterrupt();
  uint8_t buttons = mcp.readGPIO();
  if (buttons != 0xff) {
    int value = (~buttons) & 0xff;
    for (int button = 0 ; button < BUTTONS ; button++) {
      if (bit_set(value, button)) {
        bool state = buttonState[button] ? LOW : HIGH;
        buttonState[button] = !buttonState[button];
        Serial.print("buttons: ");
        Serial.print(CHILD_ID + button);
        Serial.print(" ");
        Serial.println(state);
        send(msg.setSensor(CHILD_ID + button).set(state));
      }
    }
  }
  int batteryLevel = getBatteryCapacity();
  sendBatteryLevel(batteryLevel);
  Serial.print("transmitted battery level OK: ");
  Serial.print(batteryLevel);
  Serial.println("%");
}

